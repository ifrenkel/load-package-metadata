# frozen_string_literal: true

require 'csv'

def process(row_arr)
  row_arr.map do |v|
    v = 'not set' if v.nil? || v.empty?
    v
  end
end

def parse(row_arr)
  row = process(row_arr)
  pkgraw, lic = row
  arr = pkgraw.split(':').map { |part| part.gsub(/\n|\t|'|"/, '').strip }
  lic = lic.gsub(/\n|\t|'|"/, '').strip
  pkg = arr[0..-2].join(':')[0..254]
  pkgver = arr[-1][0..254]

  if pkg[0] == ' ' || pkgver[0] == ' '
    error("formatting erro on packager", pkg, pkgver)
    return
  elsif lic[0] == ' '
    error("formatting error on license", lic)
    return
  end

  { name: pkg, spdx_identifier: lic[0..49], version: pkgver }
end

def ingest_csv(file, slice_size: 100, max_rows: 1_000_000_000, &blk)
  csv = CSV.open(file)
  num_ingested = 0
  slice_size = slice_size > max_rows ? max_rows : slice_size
  csv
    .each_slice(slice_size) do |rowslice|
      break if num_ingested >= max_rows || rowslice.nil?

      yield rowslice
        .map { |row| parse(row) }
        .select { |row| !row.nil? }
      num_ingested += slice_size
    rescue StandardError => err
      error("error", err)
    end
end
