# frozen_string_literal: true
#
def log(level, *args)
  $stdout.puts("#{Time.now} - #{level}: #{args.join(' ')}")
end

def error(*args)
  warn("#{Time.now} - error: #{args.join(' ')}")
end

def debug(*args)
  log("debug", args)
end

def info(*args)
  log("info", args)
end

@counters = {}
Counter = Struct.new(:num_seconds, :last_log_t)

def debug_every_x_seconds(message, key, num_seconds)
  t = Time.now

  unless @counters.has_key?(key)
    @counters[key] = Counter.new(num_seconds, t)
    debug(message)
    return
  end

  return if Time.now - @counters[key].last_log_t < num_seconds

  @counters[key].last_log_t = t

  debug(message)
end
