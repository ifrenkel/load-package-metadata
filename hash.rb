# frozen_string_literal: true
#
require 'digest'

def rotate(charnum, offset, first, last)
  oo = offset % (last - first + 1)
  cc = (charnum + oo) % (last + 1)
  if cc < first
    cc % first + first
  elsif cc > last
    cc % last
  else
    cc
  end
end

def hash_package_name(pkg_name, purl_type)
  offset = purl_type.to_i - 1
  pkg_name.unpack('c*').map do |char|
    cc = char.ord
    case cc
    when ('a'.ord..'z'.ord)
      rotate(cc, offset, 'a'.ord, 'z'.ord)
    when ('A'.ord..'Z'.ord)
      rotate(cc, offset, 'A'.ord, 'Z'.ord)
    when ('0'.ord..'9'.ord)
      rotate(cc, offset, '0'.ord, '9'.ord)
    else
      cc
    end
  end.pack('c*')
end
