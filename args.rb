# frozen_string_literal: true

def parse_command_line(args, opts)
  loop do
    break if args.count == 0

    key, val = args.shift(2)

    unless key[0] == '-'
      error("option needs to be first, got #{key}")
      exit 1
    end

    key = key[1..].to_sym

    unless opts.has_key?(key)
      error("invalid argument: #{key}")
      exit 1
    end

    opts[key] = val
  end

  opts
end
