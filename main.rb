# frozen_string_literal: true

require 'byebug'
require_relative './args'
require_relative './csv'
require_relative './db'
require_relative './hash'
require_relative './log'

defaults = { purl_type: 1, csv_path: './licenses.csv', slice_size: 100,
            max_rows: 1_000_000_000_000, table_prefix: '', hash_package: 'false' }

opts = parse_command_line(ARGV, defaults)

t = Time.now
info("(#{t}) start")

num_processed = 0
pmap = {}
pvmap = {}
lmap = {}

ingest_csv(opts[:csv_path], slice_size: opts[:slice_size].to_i, max_rows: opts[:max_rows].to_i) do |raw_rows|
  rows = raw_rows.map do |raw_row|
    row = raw_row
    row[:name] = opts[:hash_package] == 'yes' ? hash_package_name(raw_row[:name], opts[:purl_type]) : row[:name]
    row
  end

  num_processed += rows.count

  debug_every_x_seconds("rows processed: #{num_processed}", 'rows processed', 1)

  pkgs = rows.map do |row|
    { purl_type: opts[:purl_type], name: row[:name] }
  end
  sql = insert("#{opts[:table_prefix]}pm_packages", pkgs)
  result = run(sql)
  pmap.merge!(result.each_with_object({}) do |res, acc|
    acc[res['name']] = res['id']
  end)

  versions = rows.map do |row|
    pkgid = pmap[row[:name]]
    { purl_type: opts[:purl_type], pm_package_id: pkgid, version: row[:version] }
  end
  sql = insert("#{opts[:table_prefix]}pm_package_versions", versions)
  result = run(sql)
  pvmap.merge!(result.each_with_object({}) do |res, acc|
    acc[res['pm_package_id']] ||= {}
    acc[res['pm_package_id']][res['version']] = res['id']
  end)

  licenses = rows.map do |row|
    { spdx_identifier: row[:spdx_identifier] }
  end
  sql = insert("#{opts[:table_prefix]}pm_licenses", licenses)
  result = run(sql)
  lmap.merge!(result.each_with_object({}) do |res, acc|
    acc[res['spdx_identifier']] = res['id']
  end)

  pvlicenses = rows.map do |row|
    pkgid = pmap[row[:name]]
    pkgverid = pvmap.dig(pkgid, row[:version])
    if pkgverid.nil? || pkgverid.empty?
      error("invalid pkgverid", row)
      {}
    end

    { purl_type: opts[:purl_type], pm_package_version_id: pkgverid, pm_license_id: lmap[row[:spdx_identifier]] }
  end

  sql = insert("#{opts[:table_prefix]}pm_package_version_licenses", pvlicenses, returning_id: false)
  run(sql)
rescue StandardError => err
  error("general error", err.to_s, err.backtrace)
end

info("rows processed: #{num_processed}")
tt = Time.now
info("elapsed: #{tt - t}")
