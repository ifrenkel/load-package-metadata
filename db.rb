# frozen_string_literal: true

require 'pg'

def insert(table, attributes, returning_id: true)
  # rubocop:disable Style/StringConcatenation
  keys = attributes.first.keys
  cols = "(#{keys.join(',')})"
  vals = attributes.map { |attrs| "(" + attrs.values.map { |v| "'#{v}'" }.join(',') + ")" }.uniq.join(', ')
  ids = returning_id ? ['id'] : []
  returning = (ids + keys).join(',')
  update = keys.map { |k| "#{k}=excluded.#{k}" }.join(', ')
  format("insert into %s %s values %s on conflict %s do update set %s returning %s",
         table, cols, vals, cols, update, returning)
  # rubocop:enable Style/StringConcatenation
end

@errors = []
@pg_conn = nil
def run(sql)
  @pg_conn ||= PG.connect(dbname: 'gitlabhq_development',
                          host: "#{ENV['GDK_DIR']}/postgresql/", port: 5432)
  @pg_conn.exec(sql)
rescue StandardError => err
  error('invalid sql', sql, err.to_s)
  raise
end
