# Script for loading package metadata db from license csv

This is a helper script for loading data from csv license data into the
package metadata tables in the GitLab database.

This script is mainly done to facilitate testing for this issue:
https://gitlab.com/gitlab-org/gitlab/-/issues/382567
